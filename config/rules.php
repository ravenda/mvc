<?php

$rules = array(
                'user/login'        =>  array(
                                            'email_address' =>  'required|email',
                                            'password'      =>  'required'
                                        ),
                'user/register'     =>  array(
                                            'email_address' =>  'required|email|unique[users.email_address]',
                                            'password'      =>  'required|minlen:6|maxlen:12',
                                            'confirmpass'   =>  'required|match=password',
                                            'first_name'    =>  'required|alpha_s',
                                            'middle_name'   =>  'required|alpha_s',
                                            'last_name'     =>  'required|alpha_s',
                                            'address'       =>  'required|alpha_num_s',
                                            'type'          =>  'equal=none',
                                            'contact_number'=>  'required|phone'
                                        ),
                'user/details'      =>  array(
                                            'email_address' =>  'required|email|unique[users.email_address]',
                                            'password'      =>  'required|minlen:6|maxlen:12',
                                            'confirmpass'   =>  'required|match=password',
                                            'first_name'    =>  'required|alpha_s',
                                            'middle_name'   =>  'required|alpha_s',
                                            'last_name'     =>  'required|alpha_s',
                                            'address'       =>  'required|alpha_num_s',
                                            'contact_number'=>  'required|phone'
                                        ),
                'contact/add'       =>  array(
                                            'name'          =>  'required|alpha_s',
                                            'phone_number'  =>  'required|phone',
                                            'address'       =>  'alpha_num_s'
                                        ),
                'contact/details'   =>  array(
                                            'name'          =>  'required|alpha_s',
                                            'phone_number'  =>  'required|phone',
                                            'address'       =>  'alpha_num_s'
                                        )
            );