<?php

$error['required']       = 'The {field} field is required.';
$error['email']          = 'The {field} field must contain a valid email address.';
$error['minlen']         = 'The {field} field must be at least {param} characters in length.';
$error['maxlen']         = 'The {field} field cannot exceed {param} characters in length.';
$error['alpha']          = 'The {field} field may only contain alphabetical characters.';
$error['alpha_s']        = 'The {field} field may only contain alphabetical characters and spaces.';
$error['alpha_num']      = 'The {field} field may only contain alpha-numeric characters.';
$error['alpha_num_s']    = 'The {field} field may only contain alpha-numeric characters and spaces.';
$error['num']            = 'The {field} field must contain only numbers.';
$error['match']          = 'The {field} field does not match the {param} field.';
$error['equal']          = 'The {field} field must not be equal to {param}.';
$error['unique']         = 'The {field} field must contain a unique value.';
$error['phone']          = 'The {field} field must contain a valid phone number.';