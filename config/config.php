<?php

$config = array(
                'base_url'              =>      '',
                'default_controller'    =>      'contact',
                'default_view'          =>      'index',
                'host'                  =>      'localhost',
                'username'              =>      'root',
                'password'              =>      'root',
                'db'                    =>      'mvc',
                'port'                  =>      '80',
                'charset'               =>      'utf-8',
                'model_dir'             =>      'models',
                'view_dir'              =>      'views',
                'controller_dir'        =>      'controllers',
                'rules'                 =>      array('required', 'maxlen', 'minlen', 'unique', 'match', 'equal', 'num', 'email', 'alpha', 'alpha_s', 'alpha_num', 'alpha_num_s', 'phone')
        );