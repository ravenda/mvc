<?php

$search = config_item('controller_dir') . '/*.php';

$controllers = array();

foreach(glob($search) as $file)
{
    require_once($file);

    $namespace = get_controller($file);

    $class = new $namespace();

    $controllers[get_controller($file)] = get_class_methods($class);
}

$controller = get_controller_uri();
$view = get_view_uri();
$params = get_params_uri();

$class = config_item('controller_dir') . '\\' . ucfirst($controller);

if (array_key_exists($class, $controllers))
{
    if(in_array($view, $controllers[$class]))
    {
        route($controller, $view, $params);
    }
    else
    {
        route('error', 'show_404', $params);
    }

}
else
{
    route('error', 'show_404', $params);
}