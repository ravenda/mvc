<?php

namespace models;

require_once('core/Model.php');

use core\Model;

class Users extends Model
{
    public function __construct()
    {
        parent::__construct();
    }

    public function add_user($data)
    {
        $data['created_at'] = date('Y-m-d H:i:s');

        $query = $this->db->insert('users',$data);

        return ($this->db->affected_rows()) ? $this->db->insert_id() : false;
    }

    public function update_user($data)
    {
        extract($data);

        unset($data['id']);
        $data['update_at'] = date('Y-m-d H:i:s');

        $query = $this->db->where('id', $id)
                          ->update('users', $data);

        return ($this->db->affected_rows()) ? true : false;
    }

    public function delete_user($data)
    {
        extract($data);

        $data['deleted_flag'] = DELETED;

        $this->db->where('id', $id)
                 ->update('users', $data);

        return ($this->db->affected_rows()) ? true : false;
    }

    public function get_user_details_by_email($data)
    {
        extract($data);

        $query = $this->db->where('email_address', $email_address)
                          ->get('users');

        return ($query) ? $query->row() : false;
    }

    public function get_user_details_by_id($id)
    {
        $query = $this->db->where('id', $id)
                          ->get('users');

        return ($query) ? $query->row() : false;
    }

    public function check_credentials($data)
    {
        extract($data);

        $user = $this->get_user_details_by_email($data);

        if(!$user)
        {
            return false;
        }
        else
        {
            return (password_verify($password, $user->password)) ? $user->id : false;
        }
    }

    public function get_all_users()
    {
        $query = $this->db->where('type', 'user')
                          ->where('deleted_flag', ACTIVE)
                          ->get('users');

        return ($query) ? $query->result() : false;
    }

}
