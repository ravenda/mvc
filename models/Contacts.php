<?php

namespace models;

require_once('core/Model.php');

use core\Model;

class Contacts extends Model
{
    public function __construct()
    {
        parent::__construct();
    }

    public function add_contact($data)
    {
        $data['created_at'] = date('Y-m-d H:i:s');

        $query = $this->db->insert('contacts',$data);

        return ($this->db->affected_rows()) ? $this->db->insert_id() : false;
    }

    public function update_contact($data)
    {
        extract($data);

        unset($data['id']);
        $data['update_at'] = date('Y-m-d H:i:s');

        $query = $this->db->where('id', $id)
                          ->update('contacts', $data);

        return ($this->db->affected_rows()) ? true : false;
    }

    public function delete_contact($data)
    {
        extract($data);

        $data['deleted_flag'] = DELETED;

        $this->db->where('id', $id)
                 ->update('contacts', $data);

        return ($this->db->affected_rows()) ? true : false;
    }

    public function get_contact_details_by_id($id)
    {
        $query = $this->db->where('id', $id)
                          ->get('contacts');

        return ($query) ? $query->row() : false;
    }

    public function get_all_my_contacts($id)
    {
        $query = $this->db->where('user_id', $id)
                          ->where('deleted_flag', ACTIVE)
                          ->get('contacts');

        return ($query) ? $query->result() : false;
    }

}
