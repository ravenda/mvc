<?php

namespace core;

class Database
{
    private $_query;
    private $_stmt;
    private $_select;
    private $_from;
    private $_where = array();
    private $_or_where = array();
    private $_join = array();
    private $_group_by = array();
    private $_order_by = array();
    private $_limit;
    private $_offset;

    protected $_errors = array();

    private $_pdo;

    private $_host;
    private $_username;
    private $_password;
    private $_db;
    private $_port;
    private $_charset;

    public function __construct( $host = null, $username = null, $password = null, $db = null, $port = null, $charset = 'utf-8' )
    {
        if(is_array($host))
        {
            foreach ($host as $key => $val)
            {
                $$key = $val;
            }
        }

        $this->_host = $host;
        $this->_username = $username;
        $this->_password = $password;
        $this->_db = $db;
        $this->_port = $port;
        $this->_charset = $charset;

        $this->_connect();
    }

    private function _connect()
    {
        $dsn = 'mysql:host=' . $this->_host . ';dbname=' . $this->_db;

        $options = array(
            \PDO::ATTR_PERSISTENT   => true,
            \PDO::ATTR_ERRMODE      => \PDO::ERRMODE_EXCEPTION
        );

        try
        {
            $this->_pdo = new \PDO($dsn, $this->_username, $this->_password, $options);
        }
        catch(\PDOException $e)
        {
            $this->_errors[] = $e->getMessage();
        }
    }

    public function bind($param, $value, $type = null)
    {
        if (is_null($type)) {
            switch (true) {
                case is_int($value):
                    $type = \PDO::PARAM_INT;
                    break;
                case is_bool($value):
                    $type = \PDO::PARAM_BOOL;
                    break;
                case is_null($value):
                    $type = \PDO::PARAM_NULL;
                    break;
                default:
                    $type = \PDO::PARAM_STR;
            }
        }

        $this->_stmt->bindValue($param, $value, $type);
    }

    public function execute(){
        return $this->_stmt->execute();
    }

    public function result()
    {
        $this->execute();
        return $this->_stmt->fetchAll(\PDO::FETCH_OBJ);
    }

    public function result_array()
    {
        $this->execute();
        return $this->_stmt->fetchAll(\PDO::FETCH_ASSOC);
    }

    public function row()
    {
        $this->execute();
        return $this->_stmt->fetch(\PDO::FETCH_OBJ);
    }

    public function row_array()
    {
        $this->execute();
        return $this->_stmt->fetch(\PDO::FETCH_ASSOC);
    }

    public function affected_rows(){
        return $this->_stmt->rowCount();
    }

    public function insert_id()
    {
        return $this->_pdo->lastInsertId();
    }

    public function where($field, $value)
    {
        $this->_where[] = array($field => $value);

        return $this;
    }

    public function or_where($field, $value)
    {
        $this->_or_where[] = array($field => $value);

        return $this;
    }

    public function select($select = '*')
    {
        $this->_select = $select;

        return $this;
    }

    public function get($table = null, $limit = null, $offset = null)
    {
        $sql = 'SELECT ';

        if(!is_null($table))
        {
            $this->from($table);
        }
        if(!$this->_select)
        {
            $this->select();
        }

        $sql .= $this->_select;

        $sql .= $this->_from;

        if(!empty($this->_join))
        {
            for($i = 0 ; $i < count($this->_join) ; $i++)
            {
                $sql .= $this->_join[$i];
            }
        }

        $this->_query_builder($sql);

        return $this;
    }

    public function from($from)
    {
        $this->_from = ' FROM ' . $from;

        return $this;
    }

    public function limit($value, $offset = 0)
    {
        is_null($value) OR $this->_limit = (int) $value;
        empty($offset) OR $this->_offset = (int) $offset;

        return $this;
    }

    public function insert($table, $data = array())
    {
        $list = array();
        $params = array();
        $fields = array_keys($data);

        for($i = 0 ; $i < count($fields); $i++)
        {
            $fields[$i] = $fields[$i];
        }

        $sql = 'INSERT INTO ' . $table . '(' . implode(', ', $fields) . ') VALUES(';

        foreach($data as $field => $value)
        {
            $list[] = ':' . $field;
            $params[] = array(':' . $field => $value);
        }

        $sql .= implode(', ', $list) . ')';

        $this->_bind_params($sql, $params);

        return $this;
    }

    public function update($table, $data = array())
    {
        $fields = array();
        $params = array();

        foreach($data as $field => $value)
        {
            $fields[] = $field . ' = :' . $field;
            $params[] = array(':' . $field => $value);
        }

        $sql = 'UPDATE ' . $table . ' SET ' . implode(', ', $fields);

        $this->_query_builder($sql, $params);

        return $this;
    }

    public function delete($table)
    {
        $params = array();

        $sql = 'DELETE ';

        $this->from($table);

        $sql .= $this->_from;

        $this->_query_builder($sql);

        return $this;
    }

    private function _query_builder($sql, $params = array())
    {

        if(!empty($this->_where))
        {
            $sql .= ' WHERE ';
            $list = array();
            for($i = 0 ; $i < count($this->_where) ; $i++)
            {
                foreach($this->_where[$i] as $field => $value)
                {
                    $param = str_replace('.', '_', $field);

                    $list[] = $field . ' = ' . ':' . $param;
                    $params[] = array(':' . $param => $value);
                }
            }
            $sql .= implode(' AND ', $list);
        }

        if(!empty($this->_or_where))
        {
            $sql .= ' WHERE ';
            $list = array();
            for($i = 0 ; $i < count($this->_or_where) ; $i++)
            {
                foreach($this->_or_where[$i] as $field => $value)
                {
                    $param = str_replace('.', '_', $field);

                    $list[] = $field . ' = ' . ':' . $param;
                    $params[] = array(':' . $param => $value);
                }
            }
            $sql .= implode(' OR ', $list);
        }

        if(!empty($this->_order_by))
        {
            $sql .= ' ORDER BY ';

            $sql .= implode(', ', $this->_order_by);
        }

        if(!empty($this->_group_by))
        {
            $sql .= ' GROUP BY ';

            $sql .= implode(', ', $this->_group_by);
        }

        $sql .= ';';

        $this->_bind_params($sql, $params);
    }

    private function _query_reset()
    {
        $this->_where = array();
        $this->_or_where = array();
        $this->_join = array();
        $this->_group_by = array();
        $this->_order_by = array();
    }

    private function _bind_params($sql, $params)
    {
        $this->_query_reset();

        $this->_stmt = $this->_pdo->prepare($sql);

        for($i = 0 ; $i < count($params) ; $i++)
        {
            foreach($params[$i] as $param => $value)
            {
                $this->bind($param, $value);
            }
        }

        $this->execute();
    }

    public function join($table, $condition, $type = 'INNER')
    {
        $type = ' ' . trim(strtoupper($type));

        $this->_join[] = $type. ' JOIN ' . $table . ' ON ' . trim($condition);

        return $this;
    }

    public function group_by($column)
    {
        $this->_group_by[] = $column;

        return $this;
    }

    public function order_by($column,$pattern)
    {
        $this->_order_by[] = $column . ' ' . strtoupper($pattern);

        return $this;
    }

    public function get_where($table, $field, $value)
    {
        $this->where($field, $value)
             ->get($table);

        return $this;
    }

}
