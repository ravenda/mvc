<?php

namespace core;

class Config
{
    public $config = array();

    public function __construct()
    {
        $this->config = get_config();

        if(empty($this->config[ 'base_url']))
        {
            if( isset($_SERVER['HTTP_HOST']) && preg_match('/^((\[[0-9a-f:]+\])|(\d{1,3}(\.\d{1,3}){3})|[a-z0-9\-\.]+)(:\d+)?$/i', $_SERVER['HTTP_HOST' ]))
            {
                $base_url = (is_https() ? 'https' : 'http').'://'.$_SERVER['HTTP_HOST']
                    .substr($_SERVER['SCRIPT_NAME'], 0, strpos( $_SERVER['SCRIPT_NAME'], basename($_SERVER['SCRIPT_FILENAME'])));
            }
            else
            {
                $base_url = 'http://localhost/';
            }

            $this->set_item('base_url', $base_url);
        }
    }

    public function slash_item( $item )
    {
        if ( ! isset( $this->config[ $item] ) )
        {
            return null;
        }
        elseif( trim( $this->config[ $item ] ) === '' )
        {
            return '';
        }

        return rtrim( $this->config[ $item ], '/ ') . '/';
    }

    public function set_item( $item, $value )
    {
        $this->config[ $item ] = $value;
    }

    public function base_url($uri = '', $protocol = null)
    {
        $base_url = $this->slash_item('base_url');

        if (isset($protocol))
        {
            $base_url = $protocol.substr($base_url, strpos($base_url, '://'));
        }

        return $base_url.ltrim($this->_uri_string($uri), '/');
    }

    protected function _uri_string($uri)
    {
        if ($this->item('enable_query_strings') === FALSE)
        {
            if (is_array($uri))
            {
                $uri = implode('/', $uri);
            }
            return trim($uri, '/');
        }
        elseif (is_array($uri))
        {
            return http_build_query($uri);
        }

        return $uri;
    }

    public function item($item, $index = '')
    {
        if ($index == '')
        {
            return isset($this->config[$item]) ? $this->config[$item] : NULL;
        }

        return isset($this->config[$index], $this->config[$index][$item]) ? $this->config[$index][$item] : NULL;
    }

}