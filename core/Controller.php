<?php

namespace core;

class Controller
{
    private $_controller;
    private $_view;

    public $view = true;
    public $data = array();
    public $models = array();
    public $model = array();

    /**
     * Libraries
     */
    public $validation;
    public $input;

    public function __construct()
    {
        $this->input = load_class('Input', 'libraries');
        $this->validation = load_class('Validation', 'libraries');

        // print_arr($this->validation);
    }

    public function load_view($controller, $view)
    {
        extract($this->data);

        if($flashdata = sess_data('flashdata'))
        {
            extract($flashdata);
        }

        if(sess_data('is_logged_in'))
        {
            $this->models = array('users');

            $this->load_models();

            $logged_user = $this->model->users->get_user_details_by_id(sess_data('user_id'));
        }

        $header = config_item('view_dir') . '/layout/header.php';
        $body = config_item('view_dir') . '/' . $controller . '/' . $view . '.php';
        $footer = config_item('view_dir') . '/layout/footer.php';

        require_once($header);
        require_once($body);
        require_once($footer);

        sess_flashdata();
    }

    public function load_models()
    {
        $models = array();

        if(!empty($this->models))
        {
            foreach($this->models as $model)
            {
                $models[$model] = load_class(ucfirst($model), 'models');
            }

            $this->model = (object)$models;
        }
    }

}