<?php

namespace core;

class Router
{
    private $_uri;

    public function __construct()
    {
        $this->_uri = $_SERVER['REQUEST_URI'];

        $this->_check_uri();
        $this->_parse_uri();
        $this->_sanitize_uri();
    }

    public function uri_segments()
    {
        return explode('/' , $this->_uri);
    }

    private function _check_uri()
    {
        if(strpos($this->_uri, $_SERVER['SCRIPT_NAME']) === 0)
        {
            $this->_uri = substr($this->_uri, strlen($_SERVER['SCRIPT_NAME']));
        }

        elseif(strpos($this->_uri, dirname($_SERVER['SCRIPT_NAME'])) === 0)
        {
            $this->_uri = substr($this->_uri, strlen(dirname($_SERVER['SCRIPT_NAME'])));
        }
    }

    private function _parse_uri()
    {
        if(strncmp($this->_uri, '?/', 2) === 0)
        {
            $this->_uri = substr($this->_uri, 2);
        }
    }

    private function _sanitize_uri()
    {
        $parts = preg_split('#\?#i', $this->_uri, 2);

        $this->_uri = $parts[0];
        if(isset($parts[1]))
        {
            $_SERVER['QUERY_STRING'] = $parts[1];
            parse_str($_SERVER['QUERY_STRING'], $_GET);
        }
        else
        {
            $_SERVER['QUERY_STRING'] = '';
            $_GET = array();
        }
    }

}