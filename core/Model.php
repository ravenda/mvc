<?php

namespace core;

class Model
{
    public $db;

    public function __construct()
    {
        $db_config = array(
                'host'      =>  config_item('host'),
                'username'  =>  config_item('username'),
                'password'  =>  config_item('password'),
                'db'        =>  config_item('db'),
                'port'      =>  config_item('port'),
                'charset'   =>  config_item('charset')
            );

        $this->db = load_class('Database','core',$db_config);
    }

}