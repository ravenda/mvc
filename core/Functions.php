<?php

if(!function_exists('load_class'))
{
    function load_class($class, $directory = 'core', $param = null)
    {
        $classes = array();
        $file = $directory . '/' . $class . '.php';

        if(isset($classes[$class]))
        {
            return $classes[$class];
        }

        $name = false;

        if(file_exists($file))
        {
            $name = $class;

            if (class_exists($name,false) === false)
            {

                require_once($file);
            }
        }

        if($name === false)
        {
            echo 'Unable to locate the specified class: ' . $class . '.php';
            exit;
        }

        $namespace = $directory . '\\' . $name;

        $classes[$class] = isset($param) ? new $namespace($param) : new $namespace();

        return $classes[$class];
    }
}

if(!function_exists('get_controller_uri'))
{
    function get_controller_uri()
    {
        $route = load_class('Router');

        $segments = $route->uri_segments();

        return empty($segments[1]) ? config_item('default_controller') : $segments[1];
    }
}

if(!function_exists('get_view_uri'))
{
    function get_view_uri()
    {
        $route = load_class('Router');

        $segments = $route->uri_segments();

        return empty($segments[2]) ? config_item('default_view') : $segments[2];
    }
}

if(!function_exists('get_params_uri'))
{
    function get_params_uri()
    {
        $route = load_class('Router');

        $segments = $route->uri_segments();

        return array_slice($segments, 3);
    }
}

if(!function_exists('get_controller'))
{
    function get_controller($path)
    {
        $controller = rtrim(str_replace('/', '\\', $path),'.php');

        return $controller;
    }
}

if(!function_exists('config_item'))
{
    function config_item($item)
    {
        $config = get_config();

        return isset($config[$item]) ? $config[$item] : NULL;
    }
}

if(!function_exists('get_config'))
{
    function get_config()
    {
        if(empty($config))
        {
            $file_path = 'config/config.php';

            if(file_exists($file_path))
            {
                require($file_path);
            }
        }

        return $config;
    }
}

if(!function_exists('get_rules'))
{
    function get_rules()
    {
        if(empty($rules))
        {
            $file_path = 'config/rules.php';

            if(file_exists($file_path))
            {
                require($file_path);
            }
        }

        return $rules;
    }
}

if(!function_exists('get_error_messages'))
{
    function get_error_messages()
    {
        if(empty($error))
        {
            $file_path = 'config/error.php';

            if(file_exists($file_path))
            {
                require($file_path);
            }
        }

        return $error;
    }
}

if(!function_exists('base_url'))
{
    function base_url($uri = '', $protocol = NULL)
    {
        $config = load_class('Config');

        return $config->base_url($uri, $protocol);
    }
}

if(!function_exists('is_https'))
{
    function is_https()
    {
        if(!empty($_SERVER['HTTPS']) && strtolower($_SERVER['HTTPS']) !== 'off')
        {
            return true;
        }
        elseif(isset($_SERVER['HTTP_X_FORWARDED_PROTO']) && $_SERVER['HTTP_X_FORWARDED_PROTO'] === 'https')
        {
            return true;
        }
        elseif(!empty($_SERVER[ 'HTTP_FRONT_END_HTTPS' ]) && strtolower($_SERVER['HTTP_FRONT_END_HTTPS']) !== 'off')
        {
            return true;
        }

        return false;
    }
}

if(!function_exists('input'))
{
    function input($method, $params = null)
    {
        $input = load_class('Input','libraries');

        return $input->$method( $params );
    }
}

if(!function_exists('sess_data'))
{
    function sess_data($key)
    {
        $session = load_class('Session','libraries');

        return $session->sess_data($key);
    }
}

if(!function_exists('sess_set_data'))
{
    function sess_set_data($data)
    {
        $session = load_class('Session','libraries');

        return $session->set_data($data);
    }
}

if(!function_exists('sess_set_flashdata'))
{
    function sess_set_flashdata($key, $value)
    {
        $session = load_class('Session','libraries');

        $session->set_flash_data($key, $value);
    }
}

if(!function_exists('sess_flashdata'))
{
    function sess_flashdata()
    {
        if(sess_data('flashdata'))
        {
            sess_unset('flashdata');
            sess_set_data('flashdata',array());
        }
    }
}

if(!function_exists('sess_destroy'))
{
    function sess_destroy()
    {
        session_destroy();
    }
}

if(!function_exists('sess_unset'))
{
    function sess_unset($key)
    {
        unset($_SESSION[$key]);
    }
}

if(!function_exists('redirect'))
{
    function redirect($path = null)
    {
        $header = 'Location:' . (is_null($path) ? base_url() : base_url().$path);
        header($header);
        exit;
    }
}

if(!function_exists('refresh'))
{
    function refresh($path = null, $duration = 0)
    {
        $header = 'Refresh: ' . $duration . '; url=' . (is_null($path) ? base_url() : base_url().$path);
        header($header);
        exit;
    }
}

if(!function_exists('route'))
{
    function route($controller, $view, $args = array())
    {
        $class = load_class(ucfirst($controller), 'controllers');

        $class->load_models();

        if(empty($args))
        {
            call_user_func(array($class, $view));
        }
        else
        {
            call_user_func_array(array($class, $view), $args);
        }

        $class->load_view($controller, $view);
        exit;
    }
}

if(!function_exists('print_arr'))
{
    function print_arr($arr)
    {
        echo '<pre>';
        print_r($arr);
        echo '</pre>';
        die();
    }
}

if(!function_exists('dump'))
{
    function dump($data)
    {
        var_dump($data);die();
    }
}

if(!function_exists('escape'))
{
    function escape($data)
    {
        return htmlspecialchars($data);
    }
}