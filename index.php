<?php
ini_set('display_errors',1);
ini_set('display_startup_errors',1);
error_reporting(E_ALL);

define('SELF', pathinfo(__FILE__, PATHINFO_BASENAME));
define('DELETED', 1);
define('ACTIVE', 0);

session_start();

require_once('core/Functions.php');
require_once('core/Database.php');
require_once('config/routes.php');
