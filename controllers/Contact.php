<?php

namespace controllers;

require_once('core/Controller.php');

class Contact extends \core\Controller
{

    public function __construct()
    {
        parent::__construct();

        $this->models = array('contacts');
    }

    public function index()
    {
        if(sess_data('is_logged_in'))
        {
            $id = sess_data('user_id');

            $this->data['contacts'] = $this->model->contacts->get_all_my_contacts($id);
        }
        else
        {
            redirect('user/login');
        }
    }

    public function add()
    {
        if(sess_data('is_logged_in'))
        {
            if($data = $this->input->post())
            {
                if($this->validation->run($data))
                {
                    unset($data['confirmpass']);

                    $data['user_id'] = sess_data('user_id');

                    if($id = $this->model->contacts->add_contact($data))
                    {
                        sess_set_flashdata('status', true);
                        sess_set_flashdata('msg', 'Contact has been added to your list.');
                        sess_set_flashdata('row_id', $id);
                    }
                    else
                    {
                        sess_set_flashdata('status', false);
                        sess_set_flashdata('msg', 'Failed to add contact.');
                    }

                    redirect();
                }
                else
                {
                    $this->data['errors'] = $this->validation->display_errors();
                }
            }
        }
        else
        {
            redirect();
        }
    }

    public function details($id = null)
    {
        if(sess_data('is_logged_in'))
        {
            if (is_null($id) || func_num_args() > 1)
            {
                route('error','show_404');
            }

            if($data = $this->input->post())
            {
                if($this->validation->run($data))
                {
                    if($this->model->contacts->update_contact($data))
                    {
                        sess_set_flashdata('status', true);
                        sess_set_flashdata('msg', 'Contact has been updated.');
                    }
                    else
                    {
                        sess_set_flashdata('status', false);
                        sess_set_flashdata('msg', 'Failed to update contact.');
                    }

                    sess_set_flashdata('row_id', $data['id']);

                    redirect();
                }
                else
                {
                    $this->data['errors'] = $this->validation->display_errors();
                }
            }

            if(!$this->data['contact'] = $this->model->contacts->get_contact_details_by_id($id))
            {
                route('error','show_404');
            }

        }
        else
        {
            redirect();
        }
    }

    public function delete()
    {
        if($data = $this->input->post())
        {
            if($this->model->contacts->delete_contact($data))
            {
                sess_set_flashdata('status', true);
                sess_set_flashdata('msg', 'Contact has been deleted.');
            }
            else
            {
                sess_set_flashdata('status', false);
                sess_set_flashdata('msg', 'Failed to delete selected contact.');
            }
        }

        redirect();
    }
}