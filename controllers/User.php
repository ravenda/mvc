<?php

namespace controllers;

require_once('core/Controller.php');

class User extends \core\Controller
{

    public function __construct()
    {
        parent::__construct();

        $this->models = array( 'users' );
    }

    public function index()
    {
        if(sess_data('is_logged_in'))
        {
            $user = $this->model->users->get_user_details_by_id(sess_data('user_id'));

            if ($user->type === 'user')
            {
                route('error','show_404');
            }

            $this->data['users'] = $this->model->users->get_all_users();
        }
        else
        {
            redirect();
        }
    }

    public function login()
    {
        if(sess_data('is_logged_in'))
        {
            redirect();
        }
        else
        {
            if($data = $this->input->post())
            {
                if($this->validation->run($data))
                {
                    if($user_id = $this->model->users->check_credentials($data))
                    {
                        $data = array(
                                'is_logged_in'  =>  true,
                                'user_id'       =>  $user_id
                            );

                        sess_set_data($data);

                        $this->data['status'] = true;

                        refresh(null, 0);
                    }
                    else
                    {
                        $this->data['status'] = false;
                    }
                }
                else
                {
                    $this->data['errors'] = $this->validation->display_errors();
                }
            }
        }
    }

    public function register()
    {
        if(sess_data('is_logged_in'))
        {
            redirect();
        }
        else
        {
            if($data = $this->input->post())
            {
                if($this->validation->run($data))
                {
                    unset($data['confirmpass']);

                    $data['password'] = password_hash($data['password'], PASSWORD_DEFAULT);

                    if($this->model->users->add_user($data))
                    {
                        $this->data['status'] = true;
                    }
                    else
                    {
                        $this->data['status'] = false;
                    }
                }
                else
                {
                    $this->data['errors'] = $this->validation->display_errors();
                }
            }
        }
    }

    public function details($id = null)
    {
        if(sess_data('is_logged_in'))
        {
            $user = $this->model->users->get_user_details_by_id(sess_data('user_id'));

            if (is_null($id) || func_num_args() > 1 || ($user->id !== $id && $user->type === 'user'))
            {
                route('error','show_404');
                exit;
            }

            if($data = $this->input->post())
            {
                if($this->validation->run($data))
                {
                    unset($data['confirmpass']);
                    $data['password'] = password_hash($data['password'], PASSWORD_DEFAULT);

                    if($this->model->users->update_user($data))
                    {
                        sess_set_flashdata('status', true);
                        sess_set_flashdata('msg', 'User has been updated.');
                    }
                    else
                    {
                        sess_set_flashdata('status', false);
                        sess_set_flashdata('msg', 'Failed to update user.');
                    }

                    sess_set_flashdata('row_id', $data['id']);

                    redirect('user');
                }
                else
                {
                    $this->data['errors'] = $this->validation->display_errors();
                }
            }

            if(!$this->data['user'] = $this->model->users->get_user_details_by_id($id))
            {
                route('error','show_404');
                exit;
            }

        }
        else
        {
            redirect();
        }
    }

    public function logout()
    {
        sess_destroy();

        redirect();
    }

    public function delete()
    {
        if($data = $this->input->post())
        {
            if($this->model->users->delete_user($data))
            {
                sess_set_flashdata('status', true);
                sess_set_flashdata('msg', 'User has been deleted.');
            }
            else
            {
                sess_set_flashdata('status', false);
                sess_set_flashdata('msg', 'Failed to delete selected user.');
            }
        }

        redirect('user');
    }
}