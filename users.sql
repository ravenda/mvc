-- phpMyAdmin SQL Dump
-- version 4.0.10deb1
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Sep 22, 2015 at 07:01 PM
-- Server version: 5.5.44-0ubuntu0.14.04.1
-- PHP Version: 5.5.9-1ubuntu4.11

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `mvc`
--

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE IF NOT EXISTS `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `email_address` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `first_name` varchar(255) NOT NULL,
  `middle_name` varchar(255) NOT NULL,
  `last_name` varchar(255) NOT NULL,
  `address` varchar(255) NOT NULL,
  `contact_number` varchar(255) NOT NULL,
  `type` varchar(5) NOT NULL,
  `deleted_flag` tinyint(4) NOT NULL,
  `created_at` datetime NOT NULL,
  `update_at` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=19 ;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `email_address`, `password`, `first_name`, `middle_name`, `last_name`, `address`, `contact_number`, `type`, `deleted_flag`, `created_at`, `update_at`) VALUES
(5, 'ravenda900@gmail.com', '$2y$10$3O9etXnZsbnMDQYBLHgNvOv7nV0XigZVNK.YvUlXCGCqJNFgterUG', 'Petter', 'Bantilan', 'Amaleona', 'Rizal Ave. Ext. Highway Tagunol Cebu City', '09225394894', 'admin', 0, '2015-09-22 11:26:09', '2015-09-22 15:57:13'),
(7, 'osetzob@avufa.org', '$2y$10$eE4QHgU7azmbPfvNyvTmo.kQPBgydKZvWljIElDsRZOEtFr1A6tmS', 'Lucille', 'Price', 'Black', '1099 Tadog Way', '(717) 319-6517', 'user', 0, '2015-09-22 19:00:09', '0000-00-00 00:00:00'),
(8, 'eblulliz@hebowodo.com', '$2y$10$SuV8JbJM3PuN82lh9q2Fo.L58SnK3ihgcXCGl.YcK1ZPAwAuY0JUi', 'Lenora', 'Glover', 'Caldwell', '310 Baviwe Lane', '(567) 545-8571', 'user', 0, '2015-09-22 19:00:12', '0000-00-00 00:00:00'),
(9, 'kigpogvil@po.org', '$2y$10$ypvdY4fUaMQQw3OcIOyDi.LG5WQgghWpG19LQDLvZkZ5AD6XwIYLm', 'Irene', 'White', 'Stokes', '1571 Jaga Manor', '(427) 868-6711', 'user', 0, '2015-09-22 19:00:13', '0000-00-00 00:00:00'),
(10, 'pev@eg.edu', '$2y$10$erkR6Y17/HZDROC61Eo7cOGVEPwcOaYuTeKR4UFpN4DojCe9R2T22', 'Lettie', 'Fuller', 'Munoz', '1731 Obep Path', '(659) 497-4860', 'user', 0, '2015-09-22 19:00:15', '0000-00-00 00:00:00'),
(11, 'wowhuti@luvop.edu', '$2y$10$e6W/ANNeRhw1GZaSStZTpu6ZS99F80ts5xcd7.huVV1gNlcKn0Uay', 'Roy', 'Miller', 'Gray', '1036 Fopa Lane', '(368) 343-2277', 'user', 0, '2015-09-22 19:00:16', '0000-00-00 00:00:00'),
(12, 'caal@hahpi.gov', '$2y$10$WgV8GHzoC/GPIXSPGT3Rgetjjomt68DS43j2SFzJ.Wnx33pdHrFDa', 'Ora', 'Matthews', 'Zimmerman', '1290 Mehot Way', '(930) 367-2662', 'user', 0, '2015-09-22 19:00:18', '0000-00-00 00:00:00'),
(13, 'gihiz@bitfogif.com', '$2y$10$MypLJFN4u2nZP.yR8HBQaeeWWpR3zes4PMc2W/c.2EJ2OT/cs38x.', 'George', 'Delgado', 'Lewis', '1652 Pigef Court', '(789) 530-2843', 'user', 0, '2015-09-22 19:00:21', '0000-00-00 00:00:00'),
(14, 'wotedoji@bagusked.edu', '$2y$10$U6gIuhSQUJtzpKWPMb/dRucC0ovnmmMd0taYKQwMHzLGWXKIlYT/K', 'Tillie', 'Evans', 'Hardy', '1742 Coog Avenue', '(313) 448-7246', 'user', 0, '2015-09-22 19:00:23', '0000-00-00 00:00:00'),
(15, 'lowponu@atubioc.gov', '$2y$10$2qtp1DakYDHXr5RII2u4YOqoiEm6qTmRT2B0KDH7t8SZogA7qq2x.', 'Evan', 'Ballard', 'Ferguson', '1229 Izuhoh Grove', '(639) 269-3764', 'user', 0, '2015-09-22 19:00:25', '0000-00-00 00:00:00'),
(16, 'lafe@dowjur.net', '$2y$10$.q6qv9ykns09nrDgOi7hVesZSZnoMjjQqBTpX4IL0OsDO83UHBPPa', 'Christopher', 'Jensen', 'Dean', '1879 Firuf Terrace', '(574) 267-4485', 'user', 0, '2015-09-22 19:00:28', '0000-00-00 00:00:00'),
(17, 'he@wacvunfe.net', '$2y$10$wX5zUSwUvvA3y9AwdRmdtOVy8vNMdnQX1nHEEgH9mRm3BTl8ELR7W', 'Elmer', 'Garner', 'Buchanan', '70 Seir Mill', '(622) 240-2261', 'user', 0, '2015-09-22 19:00:30', '0000-00-00 00:00:00'),
(18, 'mam@tezuh.io', '$2y$10$CM9aO/1AoQM4p6gcJtV2r.QHX1Zwfkk2PIZa.STq98OIYFYkkEFKu', 'Lucille', 'Curtis', 'Rice', '1282 Karvoj Drive', '(900) 301-9741', 'user', 0, '2015-09-22 19:00:32', '0000-00-00 00:00:00');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
