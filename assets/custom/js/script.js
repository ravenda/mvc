$(function($){
    var obj = {};

    obj.controller = function(){
        return $('body').data('controller');
    }

    $('.generate').click(function(e){
        e.preventDefault();

        switch(obj.controller())
        {
            case 'user':
                        var first_name = chance.first();
                        $('#email_address').val(chance.email());
                        $('#password').val(first_name);
                        $('#confirmpass').val(first_name);
                        $('#first_name').val(first_name);
                        $('#middle_name').val(chance.last());
                        $('#last_name').val(chance.last());
                        $('#address').val(chance.address());
                        $('#contact_number').val(chance.phone());

                        $('#type option').filter(function(i, e){
                            return $(e).val() === "user"
                        }).prop('selected', true);
                        break;
            case 'contact':
                        $('#name').val(chance.name());
                        $('#phone_number').val(chance.phone());
                        $('#address').val(chance.address());
                        break;
        }
    });

});