<!DOCTYPE html>
<html lang="en">
<head>
    <meta http-equiv="content-type" content="text/html; charset=UTF-8">
    <meta charset="utf-8">
    <title>MVC Activity</title>
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">

    <!-- Bootstrap -->
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/plugins/bootstrap/css/bootstrap.min.css">
    <!-- Font Awesome -->
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/plugins/font-awesome/css/font-awesome.min.css">
    <!-- Lato -->
    <link href='http://fonts.googleapis.com/css?family=Lato&subset=latin,latin-ext' rel='stylesheet' type='text/css'>
    <!-- Custom -->
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/custom/css/style.css">

    <!--[if lt IE 9]>
        <script src="//html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->
</head>
<body data-url="<?php echo base_url(); ?>" data-controller="<?php echo $controller; ?>">
    <nav class="navbar navbar-default">
      <div class="container-fluid">
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header">
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" href="<?php echo base_url(); ?>">MVC</a>
        </div>

        <!-- Collect the nav links, forms, and other content for toggling -->
        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
          <ul class="nav navbar-nav">
          <?php if(sess_data('is_logged_in')): ?>
            <li>
                <a href="<?php echo base_url(); ?>"><span class="glyphicon glyphicon-earphone"></span> My Contacts</a>
            </li>
            <?php if($logged_user->type === 'admin'): ?>
            <li>
                <a href="<?php echo base_url('user'); ?>"><span class="glyphicon glyphicon-user"></span> All Users</a>
            </li>
            <?php endif; ?>
          <?php endif; ?>
          </ul>
          <?php if(sess_data('is_logged_in')): ?>
          <ul class="nav navbar-nav navbar-right">
            <li class="dropdown">
              <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><span class="caret"></span></a>
              <ul class="dropdown-menu">
                <li><a href="<?php echo base_url('user/details/' . sess_data('user_id')); ?>"><span class="glyphicon glyphicon-edit"></span> Edit Profile</a></li>
                <?php if($logged_user->type === 'admin'): ?>
                <li><a href="<?php echo base_url('user'); ?>"><span class="glyphicon glyphicon-user"></span> All Users</a></li>
                <?php endif; ?>
                <li><a href="<?php echo base_url(); ?>"><span class="glyphicon glyphicon-earphone"></span> My Contacts</a></li>
                <li role="separator" class="divider"></li>
                <li><a href="<?php echo base_url('user/logout'); ?>"><span class="glyphicon glyphicon-log-out"></span> Logout</a></li>
              </ul>
            </li>
          </ul>
          <p class="navbar-text navbar-right">Sign in as <?php echo $logged_user->first_name; ?></p>
          <?php else: ?>
          <a class="navbar-right navbar-btn btn btn-primary" href="<?php echo base_url('user/register'); ?>">Register</a>
          <a class="navbar-right navbar-btn btn btn-link" href="<?php echo base_url('user/login'); ?>">Login</a>
          <?php endif; ?>
        </div><!-- /.navbar-collapse -->
      </div><!-- /.container-fluid -->
    </nav>
    <div class="container">