<?php if(isset($status)): ?>
    <div class="alert alert-<?php echo ($status) ? 'success' : 'danger' ?> alert-dismissible text-center" role="alert">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <?php if($status): ?>
            <strong>Success!</strong> <?php echo $msg; ?>
        <?php else: ?>
            <strong>Error!</strong> <?php echo $msg; ?>
        <?php endif; ?>
    </div>
<?php endif; ?>
<div class="jumbotron">
    <h1>My Contacts</h1>
    <p>Add, View, Edit and Delete your contacts according to your preference...</p>
    <p><a class="btn btn-primary btn-lg" href="<?php echo base_url('contact/add'); ?>" role="button">Add now</a></p>
</div>
<div class="table-responsive">
    <table class="table table-bordered table-striped">
        <caption>
            <div class="pull-left">
                <span class="badge"><?php echo count($contacts); ?> contact<?php echo (count($contacts) > 1 ? 's' : ''); ?></span>
            </div>
            <div class="pull-right">
                <div class="btn-group btn-group-sm">
                    <a title="Add New" href="<?php echo base_url('contact/add'); ?>" class="btn btn-primary">
                        <span class="glyphicon glyphicon-plus"></span>
                        <span class="glyphicon glyphicon-earphone"></span>
                    </a>
                </div>
            </div>
        </caption>
        <thead>
            <tr>
                <th>Name</th>
                <th>Phone Number</th>
                <th>Address</th>
                <th>Actions</th>
            </tr>
        </thead>
        <tbody>
            <?php if(empty($contacts)): ?>
                <tr>
                    <td colspan="8" align="center">No contacts to display</td>
                </tr>
            <?php else: ?>
                <?php foreach($contacts as $contact): ?>
                    <tr<?php echo (isset($status) ? ((isset($row_id) && $row_id === $contact->id) ? ($status ? ' class="success"' : ' class="danger"') : '') : ''); ?>>
                        <td><?php echo escape($contact->name); ?></td>
                        <td><?php echo escape($contact->phone_number); ?></td>
                        <td><?php echo escape($contact->address); ?></td>
                        <td>
                            <form action="<?php echo base_url('contact/delete') ?>" method="post" onsubmit="return confirm('Do you really want to delete this contact?');">
                                <div class="btn-group btn-group-xs" role="group">
                                    <input type="hidden" name="id" value="<?php echo $contact->id; ?>">
                                    <a title="Edit" href="<?php echo base_url('contact/details/' . $contact->id); ?>" class="btn btn-primary">
                                        <span class="glyphicon glyphicon-edit"></span>
                                        <span class="glyphicon glyphicon-earphone"></span>
                                    </a>
                                    <button type="submit" class="btn btn-danger" title="Delete">
                                        <span class="glyphicon glyphicon-remove"></span>
                                        <span class="glyphicon glyphicon-earphone"></span>
                                    </button>
                                </div>
                            </form>
                        </td>
                    </tr>
                <?php endforeach; ?>
            <?php endif; ?>
        </tbody>
    </table>
</div>