<div class="col-md-6 col-md-offset-3">
    <form class="form-horizontal" method="post" action="<?php echo base_url(); ?>contact/details/<?php echo $contact->id; ?>">
        <input type="hidden" name="id" value="<?php echo $contact->id; ?>" />
        <h2 class="text-center">Update Details</h2>
        <div class="form-group">
            <label class="col-sm-5 control-label" for="name">Name</label>
            <div class="col-sm-7">
                <input type="text" class="form-control" name="name" id="name" value="<?php echo htmlspecialchars($contact->name); ?>" />
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-5 control-label" for="phone_number">Phone Number</label>
            <div class="col-sm-7">
                <input type="text" class="form-control" name="phone_number" id="phone_number" value="<?php echo htmlspecialchars($contact->phone_number); ?>" />
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-5 control-label" for="address">Address</label>
            <div class="col-sm-7">
                <input type="text" class="form-control" name="address" id="address" value="<?php echo htmlspecialchars($contact->address); ?>" />
            </div>
        </div>

        <div class="text-center">
            <div class="btn-group">
                <input type="submit" class="btn btn-success" value="Update" />
                <a href="<?php echo base_url(); ?>" class="btn btn-primary">View All</a>
            </div>
        </div>

        <?php if(isset($errors)): ?>
        <div class="panel panel-danger">
            <div class="panel-heading">
                <h3 class="panel-title">Errors</h3>
            </div>
            <div class="panel-body">
                <?php echo $errors; ?>
            </div>
        </div>
        <?php endif; ?>
    </form>
</div>