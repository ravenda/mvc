<div class="col-md-6 col-md-offset-3">
    <?php if(isset($status)): ?>
        <div class="alert alert-<?php echo ($status) ? 'success' : 'danger' ?> alert-dismissible" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <?php if($status): ?>
                <strong>Successful!</strong> You succesfully added a contact.
            <?php else: ?>
                <strong>Error!</strong> Adding contact failed. Please try again.
            <?php endif; ?>
        </div>
    <?php endif; ?>
    <form class="form-horizontal" method="post" action="<?php echo base_url('contact/add'); ?>">
        <h2 class="text-center"><span class="glyphicon glyphicon-user"></span> New Contact</h2>
        <div class="form-group">
            <label class="col-sm-5 control-label" for="name">Name</label>
            <div class="col-sm-7">
                <input type="text" class="form-control" name="name" id="name" />
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-5 control-label" for="phone_number">Phone Number</label>
            <div class="col-sm-7">
                <input type="text" class="form-control" name="phone_number" id="phone_number" />
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-5 control-label" for="address">Address</label>
            <div class="col-sm-7">
                <input type="text" class="form-control" name="address" id="address" />
            </div>
        </div>

        <div class="text-center">
            <div class="btn-group">
                <input type="submit" class="btn btn-success" value="Save" />
                <a class="btn btn-danger" href="<?php echo base_url('contact'); ?>">Cancel</a>
                <button type="button" class="btn btn-warning generate">Generate</button>
            </div>
        </div>

        <?php if(isset($errors)): ?>
        <div class="panel panel-danger">
            <div class="panel-heading">
                <h3 class="panel-title">Errors</h3>
            </div>
            <div class="panel-body">
                <?php echo $errors; ?>
            </div>
        </div>
        <?php endif; ?>
    </form>
</div>