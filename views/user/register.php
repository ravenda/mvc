<div class="col-md-6 col-md-offset-3">
    <?php if(isset($status)): ?>
        <div class="alert alert-<?php echo ($status) ? 'success' : 'danger' ?> alert-dismissible" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <?php if($status): ?>
                <strong>Congratulations!</strong> You registered successfully .Please click <a href="<?php echo base_url('user/login'); ?>">here</a> to login.
            <?php else: ?>
                <strong>Error!</strong> Registration failed. Please try again.
            <?php endif; ?>
        </div>
    <?php endif; ?>
    <form class="form-horizontal" method="post" action="<?php echo base_url('user/register'); ?>">
        <h2 class="text-center">Sign Up</h2>
        <div class="form-group">
            <label class="col-sm-5 control-label" for="email_address">Email Address</label>
            <div class="col-sm-7">
                <input type="text" class="form-control" name="email_address" id="email_address" value="<?php echo input('post', 'email_address') ?>" />
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-5 control-label" for="password">Password</label>
            <div class="col-sm-7">
                <input type="password" class="form-control" name="password" id="password" value="<?php echo input('post', 'password') ?>" />
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-5 control-label" for="confirmpass">Confirm Password</label>
            <div class="col-sm-7">
                <input type="password" class="form-control" name="confirmpass" id="confirmpass" value="<?php echo input('post', 'confirmpass') ?>" />
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-5 control-label" for="first_name">First Name</label>
            <div class="col-sm-7">
                <input type="text" class="form-control" name="first_name" id="first_name" value="<?php echo input('post', 'first_name') ?>" />
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-5 control-label" for="middle_name">Middle Name</label>
            <div class="col-sm-7">
                <input type="text" class="form-control" name="middle_name" id="middle_name" value="<?php echo input('post', 'middle_name') ?>" />
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-5 control-label" for="last_name">Last Name</label>
            <div class="col-sm-7">
                <input type="text" class="form-control" name="last_name" id="last_name" value="<?php echo input('post', 'last_name') ?>" />
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-5 control-label" for="address">Address</label>
            <div class="col-sm-7">
                <input type="text" class="form-control" name="address" id="address" value="<?php echo input('post', 'address') ?>" />
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-5 control-label" for="type">Type</label>
            <div class="col-sm-7">
                <select class="form-control" name="type" id="type">
                    <option value="none"<?php echo (input('post', 'type') === 'none' ? ' selected' : '') ?>>-Choose-</option>
                    <option value="admin"<?php echo (input('post', 'type') === 'admin' ? ' selected' : '') ?>>Admin</option>
                    <option value="user"<?php echo (input('post', 'type') === 'user' ? ' selected' : '') ?>>User</option>
                </select>
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-5 control-label" for="type">Contact Number</label>
            <div class="col-sm-7">
                <input type="text" class="form-control" name="contact_number" id="contact_number" value="<?php echo input('post', 'contact_number') ?>" />
            </div>
        </div>

        <div class="text-center">
            <div class="btn-group">
                <input type="submit" class="btn btn-primary" value="Register" />
                <a class="btn btn-success" href="<?php echo base_url('user/login'); ?>">Login</a>
                <button type="button" class="btn btn-warning generate">Generate</button>
            </div>
        </div>

        <?php if(isset($errors)): ?>
        <div class="panel panel-danger">
            <div class="panel-heading">
                <h3 class="panel-title">Errors</h3>
            </div>
            <div class="panel-body">
                <?php echo $errors; ?>
            </div>
        </div>
        <?php endif; ?>
    </form>
</div>