<?php if(isset($status)): ?>
    <div class="alert alert-<?php echo ($status) ? 'success' : 'danger' ?> alert-dismissible text-center" role="alert">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <?php if($status): ?>
            <strong>Success!</strong> <?php echo $msg; ?>
        <?php else: ?>
            <strong>Error!</strong> <?php echo $msg; ?>
        <?php endif; ?>
    </div>
<?php endif; ?>
<div class="jumbotron">
    <h1>All Users</h1>
    <p>Add, View, Edit and Delete users accordingly...</p>
    <p><small>Want to add contacts?</small></p>
    <p><a class="btn btn-primary btn-lg" href="<?php echo base_url('contact/add'); ?>" role="button">Try adding contacts</a></p>
</div>
<div class="table-responsive">
    <table class="table table-bordered table-striped">
        <caption class="text-right">
            <div class="pull-left">
                <span class="badge"><?php echo count($users); ?> contact<?php echo (count($users) > 1 ? 's' : ''); ?></span>
            </div>
        </caption>
        <thead>
            <tr>
                <th>Email Address</th>
                <th>First Name</th>
                <th>Middle Name</th>
                <th>Last Name</th>
                <th>Address</th>
                <th>Contact Number</th>
                <th>Actions</th>
            </tr>
        </thead>
        <tbody>
            <?php if(empty($users)): ?>
                <tr>
                    <td colspan="8" align="center">No users to display</td>
                </tr>
            <?php else: ?>
                <?php foreach($users as $user): ?>
                    <tr<?php echo (isset($status) ? ((isset($row_id) && $row_id === $user->id) ? ($status ? ' class="success"' : ' class="danger"') : '') : ''); ?>>
                        <td><?php echo escape($user->email_address); ?></td>
                        <td><?php echo escape($user->first_name); ?></td>
                        <td><?php echo escape($user->middle_name); ?></td>
                        <td><?php echo escape($user->last_name); ?></td>
                        <td><?php echo escape($user->address); ?></td>
                        <td><?php echo escape($user->contact_number); ?></td>
                        <td>
                            <form action="<?php echo base_url('user/delete') ?>" method="post" onsubmit="return confirm('Do you really want to delete this user?');">
                                <div class="btn-group btn-group-xs" role="group">
                                    <input type="hidden" name="id" value="<?php echo $user->id; ?>">
                                    <a title="Edit" href="<?php echo base_url('user/details/' . $user->id); ?>" class="btn btn-primary">
                                        <span class="glyphicon glyphicon-edit"></span>
                                        <span class="glyphicon glyphicon-user"></span>
                                    </a>
                                    <button type="submit" class="btn btn-danger" title="Delete">
                                        <span class="glyphicon glyphicon-remove"></span>
                                        <span class="glyphicon glyphicon-user"></span>
                                    </button>
                                </div>
                            </form>
                        </td>
                    </tr>
                <?php endforeach; ?>
            <?php endif; ?>
        </tbody>
    </table>
</div>