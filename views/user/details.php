<div class="col-md-6 col-md-offset-3">
    <form class="form-horizontal" method="post" action="<?php echo base_url(); ?>user/details/<?php echo $user->id; ?>">
        <input type="hidden" name="id" value="<?php echo $user->id; ?>" />
        <h2 class="text-center">Update Details</h2>
        <div class="form-group">
            <label class="col-sm-5 control-label" for="email_address">Email Address</label>
            <div class="col-sm-7">
                <input type="text" class="form-control" name="email_address" id="email_address" value="<?php echo escape($user->email_address); ?>" />
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-5 control-label" for="password">Password</label>
            <div class="col-sm-7">
                <input type="password" class="form-control" name="password" id="password" />
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-5 control-label" for="confirmpass">Confirm Password</label>
            <div class="col-sm-7">
                <input type="password" class="form-control" name="confirmpass" id="confirmpass" />
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-5 control-label" for="first_name">First Name</label>
            <div class="col-sm-7">
                <input type="text" class="form-control" name="first_name" id="first_name" value="<?php echo escape($user->first_name); ?>" />
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-5 control-label" for="middle_name">Middle Name</label>
            <div class="col-sm-7">
                <input type="text" class="form-control" name="middle_name" id="middle_name" value="<?php echo escape($user->middle_name); ?>" />
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-5 control-label" for="last_name">Last Name</label>
            <div class="col-sm-7">
                <input type="text" class="form-control" name="last_name" id="last_name" value="<?php echo escape($user->last_name); ?>" />
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-5 control-label" for="address">Address</label>
            <div class="col-sm-7">
                <input type="text" class="form-control" name="address" id="address" value="<?php echo escape($user->address); ?>" />
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-5 control-label" for="type">Contact Number</label>
            <div class="col-sm-7">
                <input type="text" class="form-control" name="contact_number" id="contact_number" value="<?php echo escape($user->contact_number); ?>" />
            </div>
        </div>

        <div class="text-center">
            <div class="btn-group">
                <input type="submit" class="btn btn-success" value="Update" />
                <a href="<?php echo base_url('contact'); ?>" class="btn btn-primary">View All</a>
            </div>
        </div>

        <?php if(isset($errors)): ?>
        <div class="panel panel-danger">
            <div class="panel-heading">
                <h3 class="panel-title">Errors</h3>
            </div>
            <div class="panel-body">
                <?php echo $errors; ?>
            </div>
        </div>
        <?php endif; ?>
    </form>
</div>