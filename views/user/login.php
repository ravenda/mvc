<form class="form-horizontal col-md-6 col-md-offset-3" method="post" action="<?php echo base_url('user/login'); ?>">
    <?php if(isset($status)): ?>
        <div class="alert alert-<?php echo ($status) ? 'success' : 'danger' ?> alert-dismissible" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <?php if($status): ?>
                <strong>Success!</strong> Please wait...
            <?php else: ?>
                <strong>Error!</strong> Invalid username/password. Please try again.
            <?php endif; ?>
        </div>
    <?php endif; ?>
    <h2 class="text-center">Sign in</h2>
    <div class="form-group">
        <label class="col-sm-5 control-label" for="email_address">Email Address</label>
        <div class="col-sm-7">
            <input type="text" class="form-control" name="email_address" id="email_address" value="<?php echo input('post', 'email_address') ?>" />
        </div>
    </div>
    <div class="form-group">
        <label class="col-sm-5 control-label" for="password">Password</label>
        <div class="col-sm-7">
            <input type="password" class="form-control" name="password" id="password" value="<?php echo input('post', 'password') ?>" />
        </div>
    </div>
    <div class="text-center">
        <div class="btn-group">
            <input type="submit" class="btn btn-primary" value="Login" />
            <a class="btn btn-success" href="<?php echo base_url('user/register') ?>">Register</a>
        </div>
    </div>

    <?php if(isset($errors)): ?>
    <div class="panel panel-danger">
        <div class="panel-heading">
            <h3 class="panel-title">Errors</h3>
        </div>
        <div class="panel-body">
            <?php echo $errors; ?>
        </div>
    </div>
    <?php endif; ?>
</form>