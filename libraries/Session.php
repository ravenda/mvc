<?php

namespace libraries;

class Session
{

    public function sess_data( $key )
    {
        return (is_null($key) && isset($_SESSION)) ? $_SESSION : (isset($_SESSION[$key]) ? $_SESSION[$key] : false);
    }

    public function set_data( $data, $value = null )
    {
        if(is_array($data))
        {
            foreach($data as $key => $value)
            {
                $_SESSION[$key] = $value;
            }
        }
        else
        {
            $_SESSION[$data] = $value;
        }
    }

    public function set_flash_data($key, $value)
    {
        $_SESSION['flashdata'][$key] = $value;
    }

}