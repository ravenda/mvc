<?php

namespace libraries;

class Validation
{
    protected $_field_data      = array();

    protected $_config_rules    = array();

    protected $_error_messages  = array();

    protected $_errors = array();
    protected $_error_string = '';

    protected $_error_prefix = '<ul>';
    protected $_error_suffix = '</ul>';

    protected $_rules = array();

    public function __construct()
    {
        $this->_config_rules = config_item('rules');
        $this->_rules = get_rules();
        $this->_error_messages = get_error_messages();
    }

    private function _validate($data)
    {
        $this->_field_data = $data;

        $this->_check_uri_segments();
    }

    private function _check_uri_segments()
    {
        $uri = get_controller_uri() . '/' . get_view_uri();

        if(array_key_exists($uri, $this->_rules))
        {
            $this->_check_field_rules($uri);
        }
    }

    private function _check_field_rules($uri)
    {
        $field_names = array_keys($this->_field_data);

        foreach($this->_field_data as $field => $value)
        {
            if(array_key_exists($field, $this->_rules[$uri]))
            {
                $rules = explode('|', $this->_rules[$uri][$field]);

                for($j = 0; $j < count($rules) ; $j++)
                {
                    if(strpos($rules[$j], ':') !== false)
                    {
                        $rule = explode(':', $rules[$j]);

                        if(!call_user_func_array(array($this, $rule[0]), $rule))
                        {
                            $error = $this->_error_messages[$rule[0]];
                            $error = str_replace('{field}', $field, $error);
                            $error = str_replace('{param}', $rule[1], $error);

                            $this->_errors[] = $error;
                            // break;
                        }
                    }
                    else if(strpos($rules[$j], '=') !== false)
                    {
                        $rule = explode('=', $rules[$j]);

                        if(!call_user_func_array(array($this, $rule[0]), array($value, $rule[1])))
                        {
                            $error = $this->_error_messages[$rule[0]];
                            $error = str_replace('{field}', $field, $error);
                            $error = str_replace('{param}', $rule[1], $error);

                            $this->_errors[] = $error;
                            // break;
                        }
                    }
                    else if(strpos($rules[$j], '[') !== false && stripos($rules[$j], ']') !== false)
                    {
                        $start = strpos($rules[$j], '[') + 1;
                        $end = strpos($rules[$j], ']');
                        $callback = substr($rules[$j], 0, $start - 1);

                        $rule = substr($rules[$j], $start, ($end - $start));

                        if(!call_user_func_array(array($this, $callback), array($value, $rule)))
                        {
                            $error = $this->_error_messages[$callback];
                            $error = str_replace('{field}', $field, $error);

                            $this->_errors[] = $error;
                            // break;
                        }
                    }
                    else
                    {
                        if(!call_user_func(array($this, $rules[$j]), $value))
                        {
                            $error = $this->_error_messages[$rules[$j]];
                            $error = str_replace('{field}', $field, $error);

                            $this->_errors[] = $error;
                            // break;
                        }
                    }
                }
            }
        }
    }

    public function run($post_data)
    {
        $this->_validate($post_data);

        return empty($this->_errors) ? true : false;
    }

    public function display_errors()
    {
        $this->_error_string .= $this->_error_prefix;

        for($i = 0 ; $i < count($this->_errors) ; $i++)
        {
            $this->_error_string .= '<li>' . $this->_errors[$i] . '</li>';
        }

        $this->_error_string .= $this->_error_suffix;
        $this->_reset_errors();

        return $this->_error_string;
    }

    private function _reset_errors()
    {
        $this->errors = array();
    }

    public function required($str)
    {
        $str = trim($str);

        return !empty($str) ? true : false;
    }

    public function match($str, $field)
    {
        return (($str === $this->_field_data[$field]) ? true : false);
    }

    public function equal($str, $field)
    {
        return (($str !== $field) ? true : false);
    }

    public function maxlen($str, $val)
    {
        return ($val >= strlen($str) ? true : false);
    }

    public function minlen($str, $val)
    {
        echo $str . '=' . $val;die();
        return ($val <= strlen($str) ? true : false);
    }

    public function email($str)
    {
        if (function_exists('idn_to_ascii') && $atpos = strpos($str, '@'))
        {
            $str = substr($str, 0, ++$atpos).idn_to_ascii(substr($str, $atpos));
        }

        return (filter_var($str, FILTER_VALIDATE_EMAIL) ? true : false);
    }

    public function alpha($str)
    {
        return ctype_alpha($str);
    }

    public function alpha_s($str)
    {
        return (preg_match('/^[a-zA-Z ]+$/i', $str) ? true: false);
    }

    public function alpha_num($str)
    {
        return ctype_alnum($str);
    }

    public function alpha_num_s($str)
    {
        return (preg_match('/^[a-zA-Z0-9\. ]+$/i', $str) ? true: false);
    }

    public function num($str)
    {
        return (preg_match('/^[\-+]?[0-9]*\.?[0-9]+$/', $str) ? true : false);
    }

    public function phone($str)
    {
        return (preg_match('/^\(\d{3}\) \d{3}-\d{4}|\d{11}|\+63\d{10}\d$/', $str) ? true: false);
    }

    public function unique($str, $field)
    {
        $model = load_class('Model');
        $data = explode('.', $field);

        $query = $model->db->get_where($data[0], $data[1], $str);

        $row = $query->result_array();

        return empty($row) ? true : false;
    }

}