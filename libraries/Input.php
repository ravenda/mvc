<?php

namespace libraries;

class Input
{

    public function get($key = null)
    {
        return (is_null($key) && isset($_GET)) ? $_GET : (isset($_GET[$key]) ? $_GET[$key] : false);
    }

    public function post($key = null)
    {
        return (is_null($key) && isset($_POST)) ? $_POST : (isset($_POST[$key]) ? $_POST[$key] : false);
    }

}